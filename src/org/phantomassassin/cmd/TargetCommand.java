package org.phantomassassin.cmd;

import java.util.ArrayList;
import java.util.Stack;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.drycell.command.DCCommand;
import org.drycell.command.InvalidArgumentException;
import org.drycell.main.DrycellPlugin;
import org.phantomassassin.args.PlayerArgument;
import org.phantomassassin.gui.PhantomGUI;
import org.phantomassassin.gui.TargetSelectorGUI;

public class TargetCommand extends DCCommand{

	public TargetCommand(DrycellPlugin plugin, String... aliases) {
		super(plugin, aliases);
		this.parameters.add(new PlayerArgument("target",true));
	}

	@Override
	public String getDefaultDescription() {
		return "Opens a GUI to target a player with Phantoms.";
	}

	@Override
	public boolean canConsoleExec() {
		return false;
	}

	@Override
	public boolean hasPermission(CommandSender sender) {
		return sender.hasPermission("phantomassassin.player")|| sender.hasPermission("phantomassassin.*");
	}

	@Override
	public void execute(CommandSender sender, Stack<String> args)
			throws InvalidArgumentException {
		Player p = (Player) sender;
		ArrayList<Object> parsed = this.parseArguments(sender, args);
		if(parsed.size() == 0){
			TargetSelectorGUI.open(p);
			return;
		}
		Player target = (Player) parsed.get(0);
		
		PhantomGUI.open(p, target);	
	}

}
