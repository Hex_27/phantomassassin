package org.phantomassassin.main;

import org.drycell.command.DCCommandManager;
import org.drycell.main.DrycellPlugin;
import org.phantomassassin.cmd.*;

public class PACommandExecutor extends DCCommandManager {

	public PACommandExecutor(PhantomAssassin plugin, String... bases) {
		super(plugin, bases);
		this.registerCommand(new TargetCommand(plugin,"target","attack"));
		this.registerCommand(new ReloadCommand(plugin,"reload"));
	}
	
	

}
