package org.phantomassassin.main;

import org.bukkit.command.CommandSender;

public enum LangOpt {
	
	COMMAND_RELOAD_SUCCESS("command.reload.success","&aReload complete."),
	COMMAND_TARGET_NO_SUCH_PLAYER("command.target.no-such-player","&cThe player you specified is either offline, or does not exist!"),
	NO_PERMISSIONS("no-permissions","&cYou don't have enough permissions to do this!"),
	GUI_TITLE("gui.title","&cPhantom Target: %target%"),
	GUI_HEALTH("gui.health","&cPhantom Health: &e%health%"),
	GUI_DAMAGE("gui.damage","&bPhantom Damage: &e%damage%"),
	GUI_NUMBER("gui.number","&aNumber of Phantoms: &e%number%"),
	GUI_PLUS("gui.plus","&a+1"),
	GUI_MINUS("gui.minus","&c-1"),
	GUI_COST("gui.cost","&c-%amount%x %type%"),
	GUI_CANCEL("gui.cancel","&cCancel attack"),
	GUI_ATTACK("gui.attack","&cProceed with attack"),
	GUI_BONUS_SELECTED("gui.bonus.selected","&a[Selected] "),
	GUI_DAWN_TITLE("gui.bonus.dawn.title","&eDawn"),
	GUI_DAWN_DESC("gui.bonus.dawn.desc","&6Your phantoms spawn with Fire Resistance."),
	GUI_BULWARK_TITLE("gui.bonus.bulwark.title","&eBulwark"),
	GUI_BULWARK_DESC("gui.bonus.bulwark.desc","&640% chance for the phantom to sustain no damage."),
	GUI_SPECTRE_TITLE("gui.bonus.spectre.title","&eSpectre"),
	GUI_SPECTRE_DESC("gui.bonus.spectre.desc","&6Your phantoms gain permanent Invisibility, and your target gets Blindness for 30 seconds."),
	GUI_SELECT_YOUR_TARGET("gui.select-target","&4Select Your Target"),
	MISC_NOT_ENOUGH_ITEMS("not-enough-items","&cYou need more %material% to launch this attack!"),
	MISC_ATTACK_LAUNCHED("attack-launched","&aPhantom Attack launched against &e%target%&c!"),
	MISC_TARGET_NOT_ONLINE("target-not-online","&cYour target went offline!"),
	
	;
	
	private String value;
	private String path;
	LangOpt(String lang){
		this.value = lang;
		this.path = this.toString().toLowerCase().replace("_",".");
	}
	
	LangOpt(String path, String lang){
		this.path = path;
		this.value = lang;
	}
	
	public static void init(PhantomAssassin plugin){
		for(LangOpt lang:LangOpt.values()){
			String val = plugin.getLang().fetchLang(lang.getPath(),lang.getValue());
			lang.setValue(val);
		}
	}
	
	public String getPath(){
		return this.path;
	}
	
	public String getValue(){
		return this.value;
	}
	
	public void setValue(String value){
		this.value = value;
	}
	
	public String parse(String... placeholders){
		PhantomAssassin plugin = (PhantomAssassin) PhantomAssassin.get();
		String parsed = this.getValue();
		
		String placeholder = "";
		
		for(int i = 0; i < placeholders.length; i++){
			if(i%2 == 0){
				placeholder = placeholders[i];
			}else{
				parsed = parsed.replaceAll(placeholder, placeholders[i]);
			}
		}
		return parsed;
	}
	
	public void send(CommandSender sender,String... placeholders){
		sender.sendMessage(parse(placeholders));
	}
	
	

}
