package org.phantomassassin.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.drycell.gui.InteractiveGUI;
import org.drycell.gui.ItemBuilder;
import org.phantomassassin.attack.PhantomAttack;
import org.phantomassassin.attack.PhantomBonus;
import org.phantomassassin.main.LangOpt;
import org.phantomassassin.main.PAConfigOption;
import org.phantomassassin.utils.ItemUtils;
import org.phantomassassin.utils.MathEval;
import org.phantomassassin.utils.MathUtil;
public class PhantomGUI {
	
	private static HashMap<UUID,PhantomAttack> settings = new HashMap<UUID,PhantomAttack>();
	
	public static void open(Player p, Player target){
		InteractiveGUI gui;
		PhantomAttack att = settings.get(p.getUniqueId());
		if(att == null){
			gui = new InteractiveGUI(LangOpt.GUI_TITLE.parse("%target%",target.getName()),27);
			
			att = new PhantomAttack(target.getUniqueId());
			att.setHealth(PAConfigOption.MIN_HEALTH.getDouble());
			att.setDamage(PAConfigOption.MIN_DAMAGE.getDouble());
			att.setNumber(PAConfigOption.MIN_NUMBER.getInt());
		}else{
			if(target != null) att.setTarget(target.getUniqueId());
			gui = new InteractiveGUI(LangOpt.GUI_TITLE.parse("%target%",Bukkit.getOfflinePlayer(att.getTarget()).getName()),27);
		}

		
		final PhantomAttack attack = att;
		
		ItemStack nil = new ItemBuilder(Material.LIGHT_GRAY_STAINED_GLASS_PANE).setName("").build();
		
		ItemBuilder plus = new ItemBuilder(Material.LIME_STAINED_GLASS_PANE)
		.setName(LangOpt.GUI_PLUS.parse());
		

		ItemBuilder minus = new ItemBuilder(Material.RED_STAINED_GLASS_PANE)
		.setName(LangOpt.GUI_MINUS.parse());
		
		//BEGIN: SETUP HEALTH
		
		ItemBuilder health = new ItemBuilder(Material.IRON_CHESTPLATE)
		.setName(LangOpt.GUI_HEALTH.parse("%health%",attack.getHealth()+""));
		
		for(Entry<Material, Integer> entry:getCost(PAConfigOption.COST_HEALTH.getStringList(), (int) attack.getHealth()).entrySet()){
			health.addLore(LangOpt.GUI_COST
					.parse("%type%",entry.getKey().toString(),
							"%amount%",""+entry.getValue()));
		}
		
		gui.getInventory().setItem(9, health.build());
		
		ItemBuilder healthPlus = plus.clone();
		
		if(attack.getHealth() < PAConfigOption.MAX_HEALTH.getDouble()){
			gui.getInventory().setItem(0, healthPlus.build());
			gui.setAction(0, new Runnable(){
				public void run(){
					attack.setHealth(attack.getHealth() + 1);
					settings.put(p.getUniqueId(),attack);
					open(p, null);
				}
			});
		}else{
			gui.getInventory().setItem(0, nil);
		}
		
		ItemBuilder healthMinus = minus.clone();

		if(attack.getHealth() > PAConfigOption.MIN_HEALTH.getDouble()){
			gui.getInventory().setItem(18, healthMinus.build());
			gui.setAction(18, new Runnable(){
				public void run(){
					attack.setHealth(attack.getHealth() - 1);
					settings.put(p.getUniqueId(),attack);
					open(p, null);
				}
			});
		}else{
			gui.getInventory().setItem(18, nil);
		}
		
		//END-HEALTH.
		
		//START-DAMAGE
		ItemBuilder damage = new ItemBuilder(Material.IRON_SWORD)
		.setName(LangOpt.GUI_DAMAGE.parse("%damage%",attack.getDamage()+""))
		;
		
		for(Entry<Material, Integer> entry:getCost(PAConfigOption.COST_DAMAGE.getStringList(), (int) attack.getDamage()).entrySet()){
			damage.addLore(LangOpt.GUI_COST
					.parse("%type%",entry.getKey().toString(),
							"%amount%",""+entry.getValue()));
		}
		
		gui.getInventory().setItem(10, damage.build());
		
		ItemBuilder damagePlus = plus.clone();
		
		if(attack.getDamage() < PAConfigOption.MAX_DAMAGE.getDouble()){
			gui.getInventory().setItem(1, damagePlus.build());
			gui.setAction(1, new Runnable(){
				public void run(){
					attack.setDamage(attack.getDamage() + 1);
					settings.put(p.getUniqueId(),attack);
					open(p, null);
				}
			});
		}else{
			gui.getInventory().setItem(1, nil);
		}
		
		ItemBuilder damageMinus = minus.clone();
		
		if(attack.getDamage() > PAConfigOption.MIN_DAMAGE.getDouble()){
			gui.getInventory().setItem(19, damageMinus.build());
			gui.setAction(19, new Runnable(){
				public void run(){
					attack.setDamage(attack.getDamage() - 1);
					settings.put(p.getUniqueId(),attack);
					open(p, null);
				}
			});
		}else{
			gui.getInventory().setItem(19, nil);
		}
		
		//END-DAMAGE
		
		//START-NUMBER
		ItemBuilder number = new ItemBuilder(Material.PHANTOM_MEMBRANE)
		.setName(LangOpt.GUI_NUMBER.parse("%number%",attack.getNumber()+""))
		;
		
		for(Entry<Material, Integer> entry:getCost(PAConfigOption.COST_NUMBER.getStringList(), (int) attack.getNumber()).entrySet()){
			number.addLore(LangOpt.GUI_COST
					.parse("%type%",entry.getKey().toString(),
							"%amount%",""+entry.getValue()));
		}
				
		gui.getInventory().setItem(11, number.build());
		
		ItemBuilder numberPlus = plus.clone();
		
		if(attack.getNumber() < PAConfigOption.MAX_NUMBER.getInt()){
			gui.getInventory().setItem(2, numberPlus.build());
			gui.setAction(2, new Runnable(){
				public void run(){
					attack.setNumber(attack.getNumber() + 1);
					settings.put(p.getUniqueId(),attack);
					open(p, null);
				}
			});
		}else{
			gui.getInventory().setItem(2, nil);
		}
		
		ItemBuilder numberMinus = minus.clone();

		
		if(attack.getNumber() > PAConfigOption.MIN_NUMBER.getInt()){
			gui.getInventory().setItem(20, numberMinus.build());
			gui.setAction(20, new Runnable(){
				public void run(){
					attack.setNumber(attack.getNumber() - 1);
					settings.put(p.getUniqueId(),attack);
					open(p, null);
				}
			});
		}else{
			gui.getInventory().setItem(20, nil);
		}
		//END-NUMBER
		
		//Bonuses
		String title = LangOpt.GUI_DAWN_TITLE.parse();
		if(attack.getBonus() == PhantomBonus.DAWN) title = LangOpt.GUI_BONUS_SELECTED.parse() + title;
		ItemBuilder dawn = new ItemBuilder(Material.MAGMA_CREAM)
		.setName(title)
		.addLore(LangOpt.GUI_DAWN_DESC.parse())
		;
		
		for(Entry<Material,Integer> entry:getCost(PAConfigOption.COST_DAWN.getStringList(),1).entrySet()){
			dawn.addLore(LangOpt.GUI_COST
					.parse("%type%",entry.getKey().toString(),
							"%amount%",""+entry.getValue()));
		}
				
		gui.getInventory().setItem(13,dawn.build());
		gui.setAction(13, new Runnable(){
			public void run(){
				if(attack.getBonus() != PhantomBonus.DAWN)
					attack.setBonus(PhantomBonus.DAWN);
				else attack.setBonus(null);
				settings.put(p.getUniqueId(),attack);
				open(p,null);
			}
		});
		
		title = LangOpt.GUI_BULWARK_TITLE.parse();
		if(attack.getBonus() == PhantomBonus.BULWARK) title = LangOpt.GUI_BONUS_SELECTED.parse() + title;
		ItemBuilder bulwark = new ItemBuilder(Material.DIAMOND_CHESTPLATE)
		.setName(title)
		.addLore(LangOpt.GUI_BULWARK_DESC.parse())
		;
		
		for(Entry<Material,Integer> entry:getCost(PAConfigOption.COST_BULWARK.getStringList(),1).entrySet()){
			bulwark.addLore(LangOpt.GUI_COST
					.parse("%type%",entry.getKey().toString(),
							"%amount%",""+entry.getValue()));
		}
				
		gui.getInventory().setItem(4,bulwark.build());
		gui.setAction(4, new Runnable(){
			public void run(){
				if(attack.getBonus() != PhantomBonus.BULWARK)
					attack.setBonus(PhantomBonus.BULWARK);
				else attack.setBonus(null);
				settings.put(p.getUniqueId(),attack);
				open(p,null);
			}
		});
		
		title = LangOpt.GUI_SPECTRE_TITLE.parse();
		if(attack.getBonus() == PhantomBonus.SPECTRE) title = LangOpt.GUI_BONUS_SELECTED.parse() + title;
		
		ItemBuilder spectre = new ItemBuilder(Material.ENDER_EYE)
		.setName(title)
		.addLore(LangOpt.GUI_SPECTRE_DESC.parse())
		;
		
		for(Entry<Material,Integer> entry:getCost(PAConfigOption.COST_SPECTRE.getStringList(),1).entrySet()){
			spectre.addLore(LangOpt.GUI_COST
					.parse("%type%",entry.getKey().toString(),
							"%amount%",""+entry.getValue()));
		}
				
		gui.getInventory().setItem(22,spectre.build());
		gui.setAction(22, new Runnable(){
			public void run(){
				if(attack.getBonus() != PhantomBonus.SPECTRE)
					attack.setBonus(PhantomBonus.SPECTRE);
				else attack.setBonus(null);
				settings.put(p.getUniqueId(),attack);
				open(p,null);
			}
		});
		
		
		//Ending buttons
		ItemStack cancel = new ItemBuilder(Material.BARRIER).setName(LangOpt.GUI_CANCEL.parse()).build();
		gui.getInventory().setItem(15, cancel);
		gui.setAction(15, new Runnable(){
			public void run(){
				p.closeInventory();
				settings.remove(p.getUniqueId());
			}
		});
		
		ItemBuilder proceed = new ItemBuilder(Material.ELYTRA).setName(LangOpt.GUI_ATTACK.parse());
		
		for(Entry<Material,Integer> entry:attack.calculateCost().entrySet()){
			proceed.addLore(LangOpt.GUI_COST
					.parse("%type%",entry.getKey().toString(),
							"%amount%",""+entry.getValue()));
		}
		
		gui.getInventory().setItem(16, proceed.build());
		gui.setAction(16, new Runnable(){
			public void run(){
				p.closeInventory();
				HashMap<Material,Integer> cost = attack.calculateCost();
				boolean hasEnough = true;
				for(Material item:cost.keySet()){
					if(!ItemUtils.hasEnough(p, item, cost.get(item))){
						hasEnough = false;
						LangOpt.MISC_NOT_ENOUGH_ITEMS.send(p, "%material%",item.toString());
					}
				}
				
				if(hasEnough){
					Player target = Bukkit.getPlayer(attack.getTarget());
					if(target == null){
						LangOpt.MISC_TARGET_NOT_ONLINE.send(p);
						return;
					}
					
					for(Material item:cost.keySet()){
						ItemUtils.removeMaterial(p, item, cost.get(item));
					}
					LangOpt.MISC_ATTACK_LAUNCHED.send(p, "%target%",target.getName());
					attack.proceed();
				}
			}
		});
		
		gui.openInventory(p);
	}
	
	public static HashMap<Material, Integer> getCost(List<String> list, int level){
		HashMap<Material, Integer> cost = new HashMap<>();
		for(String entry:list){
			String[] split = entry.split(",");
			Material mat = Material.matchMaterial(split[0]);
			int amount = (int) MathUtil.evaluateEquation(split[1].replaceAll("%level%",""+ level));
			cost.put(mat,amount);
		}
		
		return cost;
	}

}
