package org.phantomassassin.args;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.drycell.command.DCArgument;
import org.phantomassassin.main.LangOpt;

public class PlayerArgument extends DCArgument<Player> {

	public PlayerArgument(String name, boolean isOptional) {
		super(name, isOptional);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Player parse(CommandSender sender, String value) {
		
		return Bukkit.getPlayer(value);
	}

	@Override
	public String validate(CommandSender sender, String value) {
		if(Bukkit.getPlayer(value) == null) return LangOpt.COMMAND_TARGET_NO_SUCH_PLAYER.parse();
		return "";
	}

}
