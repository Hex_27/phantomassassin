package org.phantomassassin.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.drycell.gui.InteractiveGUI;
import org.drycell.gui.ItemBuilder;
import org.drycell.gui.PagedGUI;
import org.phantomassassin.attack.PhantomAttack;
import org.phantomassassin.attack.PhantomBonus;
import org.phantomassassin.main.LangOpt;
import org.phantomassassin.main.PAConfigOption;
import org.phantomassassin.utils.ItemUtils;
import org.phantomassassin.utils.MathEval;
import org.phantomassassin.utils.MathUtil;
public class TargetSelectorGUI {
	
	
	public static void open(Player p){
		PagedGUI pagedGUI = new PagedGUI(LangOpt.GUI_SELECT_YOUR_TARGET.parse());
		for(final Player target:Bukkit.getOnlinePlayers()){
			ItemStack skull = new ItemStack(Material.PLAYER_HEAD);
			SkullMeta meta = (SkullMeta) skull.getItemMeta();
			meta.setOwningPlayer(p);
			skull.setItemMeta(meta);
			
			ItemStack icon = new ItemBuilder(skull)
			.setName(ChatColor.RED + target.getName()).build();
			
			pagedGUI.registerItemAndAction(icon, new Runnable(){
				public void run(){
					p.closeInventory();
					Bukkit.dispatchCommand(p, "pa target " + target.getName());
				}
			}, p);
			
		}
		pagedGUI.openInventory(p);
	}

}
