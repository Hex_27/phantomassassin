package org.phantomassassin.main;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.drycell.config.DCConfig;

public enum PAConfigOption{

	COST_HEALTH("cost.one-unit-phantom-health", new ArrayList<String>(){{
		add("EMERALD,1*%level%");
	}}),
	COST_DAMAGE("cost.one-unit-phantom-damage", new ArrayList<String>(){{
		add("EMERALD,3*%level%");
	}}),
	COST_NUMBER("cost.number-of-phantoms", new ArrayList<String>(){{
		add("PHANTOM_MEMBRANE,10*%level%");
	}}),
	COST_DAWN("cost.dawn", new ArrayList<String>(){{
		add("MAGMA_CREAM,1");
	}}),
	COST_BULWARK("cost.bulwark", new ArrayList<String>(){{
		add("DIAMOND,1");
	}}),
	COST_SPECTRE("cost.spectre", new ArrayList<String>(){{
		add("ENDER_EYE,2");
	}}),
	MIN_HEALTH("minimum.health",20.0),
	MIN_DAMAGE("minimum.damage",5.0),
	MIN_NUMBER("minimum.number",1),
	MAX_HEALTH("maximum.health",50.0),
	MAX_DAMAGE("maximum.damage",20.0),
	MAX_NUMBER("maximum.number",10.0),
	BULWARK_CHANCE("bulwark-chance",40),
	;
	String path;
	Object value;
	
	PAConfigOption(String path, Object value){
		this.path = path;
		this.value = value;
	}
	
	public String getString(){
		return ChatColor.translateAlternateColorCodes('&', (String) value);
	}
	
	
	public String parse(String... placeholders){
		String parsed = this.getString();
		
		String placeholder = "";
		
		for(int i = 0; i < placeholders.length; i++){
			if(i%2 == 0){
				placeholder = placeholders[i];
			}else{
				parsed = parsed.replaceAll(placeholder, placeholders[i]);
			}
		}
		return parsed;
	}
	
	
	public int getInt(){
		if(value instanceof Number){
			return ((Number) value).intValue();
		}
		return (int) value;
	}
	
	public double getDouble(){
		if(value instanceof Number){
			return ((Number) value).doubleValue();
		}
		return (double) value;
	}
	
	public List<String> getStringList(){
		return (List<String>) value;
	}
	
	
	public static void loadValues(DCConfig conf){
		for(PAConfigOption option:PAConfigOption.values()){
			conf.reg(option.path, option.value);
		}
		conf.load();
		for(PAConfigOption option:PAConfigOption.values()){
			option.value = conf.get(option.path);
		}
	}
	

}
