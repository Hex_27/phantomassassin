package org.phantomassassin.cmd;

import java.util.Stack;

import org.bukkit.command.CommandSender;
import org.drycell.command.DCCommand;
import org.drycell.command.InvalidArgumentException;
import org.drycell.main.DrycellPlugin;
import org.phantomassassin.main.LangOpt;
import org.phantomassassin.main.PAConfigOption;
import org.phantomassassin.main.PhantomAssassin;

public class ReloadCommand extends DCCommand {

	public ReloadCommand(DrycellPlugin plugin, String... aliases) {
		super(plugin, aliases);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getDefaultDescription() {
		return "Reloads the config and language options";
	}

	@Override
	public boolean canConsoleExec() {
		return true;
	}

	@Override
	public boolean hasPermission(CommandSender sender) {
		// TODO Auto-generated method stub
		if(sender.isOp()) return true;
		return sender.hasPermission("phantomassassin.admin")||sender.hasPermission("phantomassassin.*");
	}

	@Override
	public void execute(CommandSender sender, Stack<String> args)
			throws InvalidArgumentException {
		PhantomAssassin plugin = PhantomAssassin.get();
		plugin.getDCConfig().reload();
		plugin.getLang().reloadLangFile();
		PAConfigOption.loadValues(plugin.getDCConfig());
		LangOpt.init(plugin);
		LangOpt.COMMAND_RELOAD_SUCCESS.send(sender);
	}

}
