package org.phantomassassin.main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.drycell.main.DrycellPlugin;
import org.phantomassassin.attack.BulwarkListener;

public class PhantomAssassin extends DrycellPlugin{
	
	private static PhantomAssassin i;
	
	@Override
	public void onEnable(){
		super.onEnable();
		i = this;
		new PACommandExecutor(this,"pa","phantomassassin");
		PAConfigOption.loadValues(this.getDCConfig());
		LangOpt.init(this);
		Bukkit.getPluginManager().registerEvents(new BulwarkListener(), this);
	}

	public static PhantomAssassin get() {
		return i;
	}

}
