package org.phantomassassin.attack;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Phantom;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.phantomassassin.main.PAConfigOption;
import org.phantomassassin.main.PhantomAssassin;
import org.phantomassassin.utils.MathUtil;

public class PhantomAttack {
	
	private int number;
	private double health;
	private double damage;
	private PhantomBonus bonus = null;
	private UUID target;
	
	private boolean started = false;
	private Location spawnLoc;
	
	public PhantomAttack(UUID target){
		this.target = target;
	}
	
	public void proceed(){
		started = true;
		findSpawnLocation();
		for(int i = 0; i < number; i++){
			Phantom phantom = (Phantom) spawnLoc.getWorld().spawnEntity(spawnLoc, EntityType.PHANTOM);
			phantom.setMaxHealth(health);
			phantom.setHealth(health);
			phantom.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).setBaseValue(damage);
			phantom.setTarget(Bukkit.getPlayer(target));
			if(bonus == PhantomBonus.DAWN) phantom.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE,99999,0));
			if(bonus == PhantomBonus.SPECTRE) phantom.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY,99999,0));
			if(bonus == PhantomBonus.BULWARK) phantom.setMetadata("bulwark", new FixedMetadataValue(PhantomAssassin.get(), ""));
		}
		if(bonus == PhantomBonus.SPECTRE) Bukkit.getPlayer(target).addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS,30*20,0));
	}
	
	public void findSpawnLocation(){
		Player p = Bukkit.getPlayer(target);
		if(p == null) return;
		Location spawnLoc;
		if(p.getLocation().getY()+20 < p.getWorld().getMaxHeight())
		 spawnLoc = p.getLocation().getBlock().getRelative(0,20,0).getLocation();
		else
			spawnLoc = p.getLocation();
		while(spawnLoc.getBlock().getType().isSolid() && spawnLoc.getY() < 250){
			spawnLoc = spawnLoc.clone().add(0,1,0);
		}
		
		if(spawnLoc.getBlock().getType().isSolid()) 
			this.spawnLoc = p.getLocation();
		else 
			this.spawnLoc = spawnLoc;
	}
	
	public HashMap<Material,Integer> calculateCost(){
		HashMap<Material,Integer> cost = new HashMap<>();
		
		for(String entry:PAConfigOption.COST_HEALTH.getStringList()){
			String[] split = entry.split(",");
			Material mat = Material.matchMaterial(split[0]);
			int amount = (int) MathUtil.evaluateEquation(split[1].replaceAll("%level%",""+ getHealth()));
			if(!cost.containsKey(mat))
				cost.put(mat,amount);
			else cost.put(mat, cost.get(mat) + amount);
		}
		for(String entry:PAConfigOption.COST_DAMAGE.getStringList()){
			String[] split = entry.split(",");
			Material mat = Material.matchMaterial(split[0]);
			int amount = (int) MathUtil.evaluateEquation(split[1].replaceAll("%level%",""+ getDamage()));
			if(!cost.containsKey(mat))
				cost.put(mat,amount);
			else cost.put(mat, cost.get(mat) + amount);
		}
		for(String entry:PAConfigOption.COST_NUMBER.getStringList()){
			String[] split = entry.split(",");
			Material mat = Material.matchMaterial(split[0]);
			int amount = (int) MathUtil.evaluateEquation(split[1].replaceAll("%level%",""+ getNumber()));
			if(!cost.containsKey(mat))
				cost.put(mat,amount);
			else cost.put(mat, cost.get(mat) + amount);
		}
		
		if(bonus == PhantomBonus.DAWN){
			for(String entry:PAConfigOption.COST_DAWN.getStringList()){
				String[] split = entry.split(",");
				Material mat = Material.matchMaterial(split[0]);
				int amount = (int) MathUtil.evaluateEquation(split[1].replaceAll("%num%",""+ getNumber()));
				if(!cost.containsKey(mat))
					cost.put(mat,amount);
				else cost.put(mat, cost.get(mat) + amount);
			}
		}else if(bonus == PhantomBonus.BULWARK){
			for(String entry:PAConfigOption.COST_BULWARK.getStringList()){
				String[] split = entry.split(",");
				Material mat = Material.matchMaterial(split[0]);
				int amount = (int) MathUtil.evaluateEquation(split[1].replaceAll("%num%",""+ getNumber()));
				if(!cost.containsKey(mat))
					cost.put(mat,amount);
				else cost.put(mat, cost.get(mat) + amount);
			}
		}else if(bonus == PhantomBonus.SPECTRE){
			for(String entry:PAConfigOption.COST_SPECTRE.getStringList()){
				String[] split = entry.split(",");
				Material mat = Material.matchMaterial(split[0]);
				int amount = (int) MathUtil.evaluateEquation(split[1].replaceAll("%num%",""+ getNumber()));
				if(!cost.containsKey(mat))
					cost.put(mat,amount);
				else cost.put(mat, cost.get(mat) + amount);
			}
		}		
		return cost;
	}
	
	/**
	 * @return the target
	 */
	public UUID getTarget() {
		return target;
	}
	/**
	 * @param target the target to set
	 */
	public void setTarget(UUID target) {
		this.target = target;
	}
	/**
	 * @return the number
	 */
	public int getNumber() {
		return number;
	}
	/**
	 * @return the health
	 */
	public double getHealth() {
		return health;
	}
	/**
	 * @return the damage
	 */
	public double getDamage() {
		return damage;
	}
	/**
	 * @param number the number to set
	 */
	public void setNumber(int number) {
		this.number = number;
	}
	/**
	 * @param health the health to set
	 */
	public void setHealth(double health) {
		this.health = health;
	}
	/**
	 * @param damage the damage to set
	 */
	public void setDamage(double damage) {
		this.damage = damage;
	}
	
	public PhantomBonus getBonus(){
		return this.bonus;
	}
	
	public void setBonus(PhantomBonus bonus){
		this.bonus = bonus;
	}
}
