package org.phantomassassin.attack;

import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.phantomassassin.main.PAConfigOption;
import org.phantomassassin.utils.MathUtil;

public class BulwarkListener implements Listener{
	
	@EventHandler
	public void onAttack(EntityDamageEvent event){
		if(event.getEntity().hasMetadata("bulwark")){
			if(MathUtil.randInt(0,100) <= PAConfigOption.BULWARK_CHANCE.getInt()){
				event.setCancelled(true);
				event.getEntity().getWorld().spawnParticle(
						Particle.DRAGON_BREATH,
						event.getEntity().getLocation(),
						5);
				event.getEntity().getWorld().playSound(event.getEntity().getLocation(),
						Sound.BLOCK_ANVIL_HIT, 1, 1);
			}
		}
	}

}
