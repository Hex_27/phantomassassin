package org.phantomassassin.utils;

import java.util.Random;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class MathUtil {

	public static double evaluateEquation(String s) {
		// ScriptEngineManager man = new ScriptEngineManager();
		// ScriptEngine jsEngine = man.getEngineByName("JavaScript");
		// try {
		// Object num = jsEngine.eval(s);
		// if(num instanceof Double){
		// num = ((Double) num).intValue();
		// }
		// return (int) num;
		// } catch (ScriptException e) {
		// return 0;
		// }
		return (double) new MathEval().evaluate(s);
	}

	public static double round(double value, int precision) {
		int scale = (int) Math.pow(10, precision);
		return (double) Math.round(value * scale) / scale;
	}

	public static int randInt(int min, int max) {

		Random rand = new Random();

		int randomNum = rand.nextInt((max - min) + 1) + min;

		return randomNum;
	}
}
